@extends('layout.header_footer')

@section('title', 'Page Title')

@section('content')
    <style>
        footer {
            width: 100%;
            height: 80px;
            background-color: #232323;
        }

        .footer-section {
            padding: 26px 40px 22px;
            overflow: hidden;
        }

        .footer-social {
            display: inline-block;
        }

        *, ::after, ::before {
            box-sizing: border-box;
        }

        html, body {
            font-family: "Poppins", sans-serif;
        }

        a {
            font-family: "Poppins", sans-serif;
            color: #007bff;
            text-decoration: none;
            background-color: transparent;
        }

        a:hover {
            color: #0056b3;
            text-decoration: underline;
        }

        a:hover, a:focus {
            text-decoration: none;
            text-decoration-line: none;
            text-decoration-style: solid;
            text-decoration-color: currentcolor;
            text-decoration-thickness: auto;
            outline: none;
            outline-color: currentcolor;
            outline-style: none;
            outline-width: medium;
        }

        .footer-social a {
            display: inline-block;
            font-size: 14px;
            color: #fff;
            margin-right: 24px;
            padding-bottom: 7px;
            position: relative;
            -webkit-transition: all 0.4s;
            -moz-transition: all 0.4s;
            -o-transition: all 0.4s;
            transition: all 0.4s;
            border-bottom: 2px solid rgba(255, 255, 255, 0.2);
        }

        .footer-social a::after {

            position: absolute;
            content: "";
            width: 0;
            height: 2px;
            left: 0;
            bottom: -2px;
            background: #080862;;
        }

        .footer-social a {
            font-size: 14px;
            color: #fff;
        }

        @media (max-width: 440px) {
            #img {
                height: 200px;
            }
        }
        .pagination{
            margin-right: 14px;
        }
        .pagination > li > a
        {
            color: #0d2235;
        }


    </style>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <img id="img" src="img/slider/Header image new.png" alt="Los Angeles">
            </div>
        </div>
    </div>
    <footer class="footer-section">
        <div class="footer-social">
            <a href="#">Facebook</a>
            <a href="https://www.youtube.com/channel/UCb02u7eQ0YziK-rzcKjjOeg" target="_blank">You Tube</a>
            <a href="/contact">Contact</a>
        </div>
    </footer>
    <section>
        <div class="grid-portfolio" id="portfolio">
            <div class="container">
                <div class="row">
                    @foreach($images as $img)
                        <div class="col-md-4 col-sm-6">
                            <div class="portfolio-item" id="">
                                <a href="img/gallery/{{$img->image_name}}" data-lightbox="image-1">
                                    <div class="thumb hover-effect">
                                        <div class="hover-effect">
                                        </div>
                                        <div class="image">
                                            <img src="img/gallery/{{$img->image_name}}">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                    <div class="text-right">
                        <div class="d-flex justify-content-center">
                            {!! $images->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
