@extends('layout.header_footer')

@section('title', 'Contact Us')

@section('content')

    <div class="page-heading">
        <div class="container">
            <div class="heading-content">
                <h1>About <em>Us</em></h1>
            </div>
        </div>
    </div>


    <div class="services">
        <div class="container">
            <div class="col-md-4 col-sm-6">
                <div class="service-item">
                    <div class="icon">
                        <a  href="tel:0757051045"><img src="img/service_2.png" alt=""></a>
                    </div>
                    <div class="text">
                        <h4>Call Us</h4>
                        <a style="font-weight: bold; color: #f4dd5b; font-size: 22px;" href="tel:0757051045">+94 757051045</a><p></p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item">
                    <div class="icon">
                        <a href="mailto:chandana.jba@gmail.com"><img src="img/service_6.png" alt=""></a>
                    </div>
                    <div class="text">
                        <h4>Email</h4>
                        <a href="mailto:chandana.jba@gmail.com" style="color: #f4dd5b; font-size: 22px;">chandana.jba@gmail.com</a>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="service-item">
                    <div class="icon">
                        <img src="img/service_3.png" alt="">
                    </div>
                    <div class="text">
                        <h4>Address</h4>
                        <p style=" color: #f4dd5b; font-size: 22px;">Parker St, LA, USA</p>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
