<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>QuoteSalad</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Montserrat">

    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">


    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="css/light-box.css">
    <link rel="stylesheet" href="css/templatemo-style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Kanit:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <style>
        @media screen and (max-width: 600px) {
            div.logo {
                display: none;
            }

            nav {
                background: rgba(0, 0, 0, 0);
                box-shadow: 0px 0px 0px 0px rgba(0, 0, 0, 0);
            }

            .menu-icon {
                background: transparent;
                border: 0px solid #fff;
                width: 48px;
                height: 30px;
                margin: 3px 39px 0 auto;
                position: relative;
                cursor: pointer;
                transition: background 0.5s;
                border-radius: 5px;
            }
        }
    </style>
</head>

<body>

<nav>
    <div class="logo">
        <a style="font-family: 'Montserrat', serif; font-weight: bold;
" href="/">Quote<em>Salad</em></a>
    </div>
    <div class="menu-icon">
        <span></span>
    </div>
</nav>

<section class="overlay-menu">
    <div class="container">
        <div class="row">
            <div class="text-center">
                <div class="main-menu">
                    <ul>
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a class="disabled" href="/contact">Contact</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


@yield('content')

<footer>
    <div class="container-fluid">
        <div class="col-md-12">
            <p>Copyright &copy; 2020 Quote Salad

                | Designed by Dul</p>
        </div>
    </div>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
</body>
</html>
