<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gallery_images extends Model
{
    public $table = "gallery_images";
    protected $fillable = ['image_name'];
}
