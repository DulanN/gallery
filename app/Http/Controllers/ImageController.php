<?php

namespace App\Http\Controllers;
use App\gallery_images;
use Illuminate\Http\Request;
use File;
use DB;

class ImageController extends Controller
{
    public function image_upload(Request $request)
    {
        $request->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $imageName = time().'.'.$request->image->extension();

        gallery_images::create([
            'image_name' => $imageName,
        ]);

        $request->image->move(public_path('img/gallery'), $imageName);

        return redirect('dashboard')->with('success','Your image has been uloaded');

    }
    public function image_delete(Request $request)
    {
        $delete = $request->delete;
        $id = $request->id;
        $image_path = "img/gallery/" . $delete;

        if (File::exists($image_path)) {
            File::delete($image_path);
        }
        DB::table('gallery_images')->where('id', $id)->delete();

        return redirect('dashboard')->with('error','Image has been deleted!');
    }

}
