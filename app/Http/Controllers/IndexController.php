<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function Index(Request $request)
    {
        $images = DB::table('gallery_images')
            ->orderBy('gallery_images.created_at','desc')
            ->simplePaginate(18);

        return view('index',compact('images'));
    }
    public function dashboard(Request $request)
    {
        $images = DB::table('gallery_images')->get();
        return view('adminpanel.dashboard',compact('images'));
    }
}
